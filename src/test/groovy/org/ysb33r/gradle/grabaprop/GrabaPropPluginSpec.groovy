//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabaprop

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.resolvers.PropertyResolver
import spock.lang.Specification

class GrabaPropPluginSpec extends Specification {
    Project project = ProjectBuilder.builder().build()

    void setup() {
        project.allprojects {
            apply plugin: 'org.ysb33r.grabaprop'
        }
    }

    void 'Apply grabaprop plugin'() {
        setup:
        def sysEnvGradle = project.grabaprop.SYSTEM_ENV_PROPERTY

        expect:
        project.extensions.getByName(GrabaPropPlugin.EXTENSION_NAME) instanceof PropertyResolver
        project.grabaprop.get('a.b.c') == null
        project.grabaprop.get('a.b.c', 'd.e.f') == 'd.e.f'
        project.grabaprop.get('a.b.c', sysEnvGradle) == null
        project.grabaprop.get('a.b.c', 'd.e.f', sysEnvGradle) == 'd.e.f'

        project.grabaprop.provide('a.b.c').getOrNull() == null
        project.grabaprop.provide('a.b.c', 'd.e.f').get() == 'd.e.f'
        project.grabaprop.provide('a.b.c', sysEnvGradle).getOrNull() == null
        project.grabaprop.provide('a.b.c', 'd.e.f', sysEnvGradle).get() == 'd.e.f'

        project.grabaprop.provideAtConfiguration('a.b.c').getOrNull() == null
        project.grabaprop.provideAtConfiguration('a.b.c', 'd.e.f').get() == 'd.e.f'
        project.grabaprop.provideAtConfiguration('a.b.c', project.grabaprop.SYSTEM_ENV_PROPERTY).getOrNull() == null
        project.grabaprop.provideAtConfiguration('a.b.c', 'd.e.f', project.grabaprop.SYSTEM_ENV_PROPERTY).get() == 'd.e.f'
    }
}