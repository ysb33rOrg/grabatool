//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabatool

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.OperatingSystem
import spock.lang.Specification

class GrabaToolPluginSpec extends Specification {

    // This line is read by the build to simplify versioning.
    final static String PRODUCT_VERSION = '1.10'

    final static String PRODUCT_NAME = 'go'
    final static File TEST_BINARY_ROOT = new File ('build/test-binaries').absoluteFile
    final static String BASE_URI = TEST_BINARY_ROOT.toURI()
    final static OperatingSystem OS = OperatingSystem.current()

    Project project = ProjectBuilder.builder().build()

    def 'Apply plugin and download Go'() {
        given:
        project.allprojects {
            apply plugin: 'org.ysb33r.grabatool'

            grabatool {
                tool(PRODUCT_NAME) {
                    // tag::uri_method_a[]
                    uri { GrabaToolUriDescriptor cfg -> // <1>
                        String platform
                        String ext = 'tar.gz'

                        if (cfg.os.isWindows()) { // <2>
                            ext = 'zip'
                            if (cfg.os.arch == OperatingSystem.Arch.X86) {
                                platform = 'windows-386'
                            } else if (cfg.os.arch == OperatingSystem.Arch.X86_64) {
                                platform = 'windows-amd64'
                            } else {
                                throw new GradleException("${cfg.os.arch} is not a supported configuration for Windows") // <3>
                            }
                        } else if (cfg.os.isLinux()) {
                            if (cfg.os.arch == OperatingSystem.Arch.X86) {
                                platform = 'linux-386'
                            } else if (cfg.os.arch == OperatingSystem.Arch.X86_64) {
                                platform = 'linux-amd64'
                            } else {
                                throw new GradleException("${cfg.os.arch} is not a supported configuration for Linux")
                            }
                        } else if (cfg.os.isMacOsX()) {
                            platform = 'darwin-amd64'
                        } else {
                            throw new GradleException("${cfg.os} is not supported")
                        }

                        // end::uri_method_a[]
                        "${BASE_URI}/go${cfg.version}.${platform}.${ext}".toURI()
                        // tag::uri_method_b[]
                    }
                    // end::uri_method_b[]

                    // tag::entrypoint_method[]
                    entrypoint { GrabaToolEntryPointDescriptor ep -> // <1>
                        String ext = ep.os.isWindows() ? '.exe' : '' // <1>
                        new File(ep.distributionRoot, "bin/${ep.name}${ext}")
                    }
                    // end::entrypoint_method[]
                }
            }
        }

        when:
        GrabaToolExtension grabatool = project.grabatool
        File root = grabatool.get(PRODUCT_NAME).version(PRODUCT_VERSION).getRoot()
        File bin = new File(root, "bin/go${OS.isWindows() ? '.exe' : ''}")

        then:
        grabatool.get(PRODUCT_NAME).version(PRODUCT_VERSION).entrypoint('go').absolutePath == bin.absolutePath
        bin.exists()
    }
}