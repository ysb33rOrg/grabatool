//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabaprop

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.core.PropertyResolveOrder
import org.ysb33r.grolifant.api.core.resolvers.PropertyResolver

/**
 * Provides the {@code grabaprop} extension.
 *
 * @author Schalk W. Cronjé
 *
 * @since 0.4.0
 */
@CompileStatic
class GrabaProp extends PropertyResolver {

    public static final PropertyResolveOrder PROJECT_SYSTEM_ENV = PropertyResolver.PROJECT_SYSTEM_ENV
    public static final PropertyResolveOrder SYSTEM_ENV_PROPERTY = PropertyResolver.SYSTEM_ENV_PROPERTY

    GrabaProp(ProjectOperations po) {
        super(po)
    }

    /**
     * Get a provider to a property which cannot be used at configuration time.
     *
     * @param name Name of property.
     * @param order Resolve order.
     * @return Provider.
     */
    Provider<String> provide(
        final String name,
        PropertyResolveOrder order
    ) {
        provide(name, null, order, false)
    }

    /**
     * Get a provider to a property which cannot be used at configuration time.
     *
     * @param name Name of property.
     * @param defaultValue Default value
     * @param order Resolve order
     * @return Provider.
     */
    Provider<String> provide(
        final String name,
        String defaultValue,
        PropertyResolveOrder order
    ) {
        provide(name, defaultValue, order, false)
    }

    /** Gets a provider to a property that is safe to use at configuration-time.
     *
     * @param name Name of property to resolve.
     * @param order Property resolver order.
     * @return Provider to a property.
     */
    Provider<String> provideAtConfiguration(final String name, PropertyResolveOrder order) {
        provide(name, null, order, true)
    }

    /** Gets a provider to a property that is safe to use at configuration-time.
     *
     * @param name Name of property to resolve.
     * @paran defaultValue Default value.
     * @param order Property resolver order.
     * @return Provider to a property.
     */
    Provider<String> provideAtConfiguration(final String name, final String defaultValue, PropertyResolveOrder order) {
        provide(name, defaultValue, order, true)
    }
}
