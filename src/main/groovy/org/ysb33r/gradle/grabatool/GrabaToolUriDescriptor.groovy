//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabatool

import groovy.transform.CompileStatic
import org.ysb33r.grolifant.api.core.OperatingSystem

/** Provides a descriptor for which a URI can be calculated.
 * @since 0.1
 */
@CompileStatic
interface GrabaToolUriDescriptor {

    /** The version of the tool.
     *
     * @return
     */
    String getVersion()

    /** The operating system that the tool applies to.
     *
     * Platform independent tools can choose to ignore this.
     *
     * @return OS description
     */
    OperatingSystem getOs()
}