//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabatool

import java.util.function.Function

/**
 * @since
 */
interface ToolConfigurator {

    void uri( Closure uriDescriptor )

    void uri( Function< GrabaToolUriDescriptor, URI> uriDescriptor )

    void entrypoint(Closure entryPointResolver)

    void entrypoint(Function< GrabaToolEntryPointDescriptor, File > entryPointResolver)

    // unpackDMGs()

    // doNotUnpack()

    // unpack( Closure unpackDescriptor )
    // unpack( Consumer< GrabaToolUnpackDescriptor > )
}