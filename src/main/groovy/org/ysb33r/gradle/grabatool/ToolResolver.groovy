//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabatool

import groovy.transform.CompileStatic
import org.ysb33r.grolifant.api.core.OperatingSystem

/**
 * @since 0.1
 */
@CompileStatic
interface ToolResolver {
    /** Returns a URI for downloading a tool
     *
     * @param os Applicable operating system. (Typically the OS Gradle is running on).
     * @param version Version of the distribution or SDK
     * @return Download URI
     */
    URI resolveURI(final OperatingSystem os, final String version )

    /** Resolves a entrypoint after a tool or SDK distribution has been downloaded.
     *
     * The system will automatically download the binaries if not already cached. (Unless Gradle is offline).
     *
     * @param os Applicable operating system. (Typically the OS Gradle is running on).
     * @param distributionRoot Root where distribution has been download and unpacked to.
     * @param name Name of entry point within distribution to resolve. Can be name of of executable or even a relative path.
     * @return Resolved file
     * @throw Can throw if distribution annot be downloaded or file was not found.
     */
    File resolveEntryPoint( final OperatingSystem os, File distributionRoot, final String name)

    // (getAndVerifyDistributionRoot)
    // File locateRootAfterUnpack(final File distDir)

    // (unpack)
    // unpackDistribution(final File srcArchive, final File destDir)
}