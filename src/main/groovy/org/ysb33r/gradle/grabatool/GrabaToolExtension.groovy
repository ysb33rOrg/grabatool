//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabatool

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.ysb33r.gradle.grabatool.internal.Tool
import org.ysb33r.gradle.grabatool.internal.ToolDownloadFactory
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations

import static groovy.lang.Closure.DELEGATE_FIRST

/** The GrabaTool extension.
 *
 * Provides registration facility for tools and SDKs as well as
 * acessors and lazy loading.
 *
 * @since 0.1
 */
@CompileStatic
class GrabaToolExtension {

    GrabaToolExtension(Project project) {
        this.projectOperations = ProjectOperations.find(project)
    }

    void tool(final String toolName, Closure configurator) {
        Closure config = (Closure) (configurator.clone())
        config.delegate = createOrGetToolByName(toolName)
        config.resolveStrategy = DELEGATE_FIRST
        config()
    }

    void tool(final String toolName, Action<ToolConfigurator> configurator) {
        configurator.execute(createOrGetToolByName(toolName))
    }

    DistributionResolver get(final String name) {
        Tool requestedTool = getToolByName(name)
        new ToolDownloadFactory(projectOperations, name, requestedTool, os)
    }

    private Tool createOrGetToolByName(final String toolName) {
        if (this.tools.containsKey(toolName)) {
            this.tools[toolName]
        } else {
            Tool t = new Tool()
            this.tools.put(toolName, t)
            t
        }
    }

    private Tool getToolByName(final String toolName) {
        if (this.tools.containsKey(toolName)) {
            this.tools[toolName]
        } else {
            throw new GrabaToolException("'${toolName}' is not a registered tool")
        }
    }

    final private ProjectOperations projectOperations
    final private Map<String, Tool> tools = [:]
    final private OperatingSystem os = OperatingSystem.current()
}
