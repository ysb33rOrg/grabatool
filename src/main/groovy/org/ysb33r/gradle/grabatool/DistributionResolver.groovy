//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabatool

import groovy.transform.CompileStatic

/** An object that can resolve a distribution by version.
 * @since 0.1
 */
@CompileStatic
interface DistributionResolver {

    /** Resolves a local path for a tool/SDK by specific version.
     *
     * @param ver Version to download.
     * @return Object that can resolve entrypoints on a distribution.
     */
    ToolPathResolver version(final String ver)
}