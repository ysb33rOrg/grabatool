//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabatool.internal

import groovy.transform.CompileStatic
import org.ysb33r.gradle.grabatool.GrabaToolEntryPointDescriptor
import org.ysb33r.gradle.grabatool.GrabaToolException
import org.ysb33r.gradle.grabatool.GrabaToolUriDescriptor
import org.ysb33r.gradle.grabatool.ToolConfigurator
import org.ysb33r.gradle.grabatool.ToolResolver
import org.ysb33r.grolifant.api.core.OperatingSystem

import java.util.function.Function

/** Description of a tool or SDK.
 *
 * @since 0.1
 */
@CompileStatic
class Tool implements ToolResolver, ToolConfigurator {

    void uri( Closure uriDescriptor ) {
        uri( uriDescriptor as Function< GrabaToolUriDescriptor, URI> )
    }

    void uri( Function< GrabaToolUriDescriptor, URI> uriDescriptor ) {
        this.uriDescriptor = uriDescriptor
    }

    void entrypoint(Closure entryPointResolver) {
        entrypoint( entryPointResolver as Function< GrabaToolEntryPointDescriptor, File >)
    }

    void entrypoint(Function< GrabaToolEntryPointDescriptor, File > entryPointResolver) {
        this.entrypointResolver = entryPointResolver
    }

    URI resolveURI( final OperatingSystem os_, final String version_ ) {
        if(uriDescriptor) {
            uriDescriptor.apply( new GrabaToolUriDescriptor() {
                @Override
                String getVersion() {
                    version_
                }

                @Override
                OperatingSystem getOs() {
                    os_
                }
            })
        } else {
            throw new GrabaToolException('URI resolver was not configured')
        }
    }

    File resolveEntryPoint( final OperatingSystem os_, File distributionRoot_, final String name_) {
        if(entrypointResolver) {
            entrypointResolver.apply(new GrabaToolEntryPointDescriptor() {
                @Override
                String getName() {
                    name_
                }

                @Override
                File getDistributionRoot() {
                    distributionRoot_
                }

                @Override
                OperatingSystem getOs() {
                    os_
                }
            })
        } else {
            throw new GrabaToolException('Entrypoint resolver was not configured')
        }

    }

    private Function< GrabaToolUriDescriptor, URI> uriDescriptor
    private Function< GrabaToolEntryPointDescriptor, File > entrypointResolver
}
