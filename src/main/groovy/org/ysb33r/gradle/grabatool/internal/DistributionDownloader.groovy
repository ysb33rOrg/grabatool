//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabatool.internal

import groovy.transform.CompileStatic
import org.gradle.internal.FileUtils
import org.ysb33r.gradle.grabatool.ToolPathResolver
import org.ysb33r.gradle.grabatool.ToolResolver
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.core.downloader.AbstractDistributionInstaller

/**
 * @since 0.1
 */
@CompileStatic
class DistributionDownloader extends AbstractDistributionInstaller implements ToolPathResolver {

    DistributionDownloader(
        final OperatingSystem os,
        final ProjectOperations projectOperations,
        final String name,
        final String version,
        final ToolResolver toolResolver
    ) {
        super(
            "Grabatool: ${name}",
            "grabatool/${FileUtils.toSafeFileName(name)}",
            projectOperations
        )
        this.toolResolver = toolResolver
        this.os = os
        this.version = version
    }

    @Override
    URI uriFromVersion(String version) {
        toolResolver.resolveURI(os, version)
    }

    @Override
    File getRoot() {
        getDistributionRoot(version).get()
    }

    @Override
    File entrypoint(String name) {
        toolResolver.resolveEntryPoint(os, getDistributionRoot(version).get(), name)
    }

    private final ToolResolver toolResolver
    private final OperatingSystem os
    private final String version
}
