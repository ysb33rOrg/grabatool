//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018 - 2022
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.grabatool.internal

import groovy.transform.CompileStatic
import org.ysb33r.gradle.grabatool.DistributionResolver
import org.ysb33r.gradle.grabatool.ToolPathResolver
import org.ysb33r.gradle.grabatool.ToolResolver
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations

/** A factory for creating downloaders of tools of a specific type.
 *
 * @since 0.1
 */
@CompileStatic
class ToolDownloadFactory implements DistributionResolver {

    ToolDownloadFactory(
        final ProjectOperations projectOperations,
        final String name,
        final ToolResolver tool,
        final OperatingSystem os
    ) {
        this.tool = tool
        this.name = name
        this.os = os
        this.projectOperations = projectOperations
    }

    @Override
    ToolPathResolver version(String ver) {
        new DistributionDownloader(os, projectOperations, name, ver, tool)
    }

    final ToolResolver tool
    final String name
    final OperatingSystem os
    final ProjectOperations projectOperations
}
